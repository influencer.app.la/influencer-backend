'use strict';

// const _ = require('lodash');


module.exports = {

  async start(ctx) {
    const { _id } = ctx.state.user;
    const body = ctx.request.body;
    const profiles = await strapi.services.profile.find({ user: _id, _limit: 1 });
    const gig = await strapi.services.gig.findOne({ id: ctx.params._id });
    gig.status = 'STARTED';
    gig.hires = body.hires;
    await gig.save();
    await strapi.services.device.sendPushNotification(
      body.hires,
      "You've been hired",
      `${profiles[0].fullname} hired you to "${gig.name}" gig`,
      { code: 'GIG_STARTED', gigId: gig.id }
    );
    return gig;
  },

  async close(ctx) {
    const { _id } = ctx.state.user;
    const body = ctx.request.body;
    for(const feedback of body.feedbacks)
      await strapi.services.feedback.create(feedback);
    const profiles = await strapi.services.profile.find({ user: _id, _limit: 1 });
    const gig = await strapi.services.gig.findOne({ id: ctx.params._id });
    gig.status = 'CLOSED';
    await gig.save();
    await strapi.services.device.sendPushNotification(
      gig.hires,
      "Gig closed",
      `${profiles[0].fullname} has closed "${gig.name}" gig`,
      { code: 'GIG_CLOSED', gigId: gig.id }
    );
    return gig;
  },

  async cancel(ctx) {
    const { _id } = ctx.state.user;
    const profiles = await strapi.services.profile.find({ user: _id, _limit: 1 });
    const gig = await strapi.services.gig.findOne({ id: ctx.params._id });
    gig.status = 'CANCELLED';
    await gig.save();
    await strapi.services.device.sendPushNotification(
      gig.applicants,
      "Gig cancelled",
      `${profiles[0].fullname} has cancelled "${gig.name}" gig`,
      { code: 'GIG_CANCELLED', gigId: gig.id }
    );
    return gig;
  },

  async createSelf(ctx) {
    const { _id } = ctx.state.user;
    const body = ctx.request.body;
    const profiles = await strapi.services.profile.find({ user: _id, _limit: 1 });
    body.owner = profiles[0]._id;
    if (typeof body.location != 'string') {
      const location = await strapi.services.location.create(body.location);
      body.location = location.id;
    }
    return await strapi.services.gig.create(body);
  },

  async findSelf(ctx) {
    const { _id } = ctx.state.user;
    const profiles = await strapi.services.profile.find({ user: _id, _limit: 1 });
    if (profiles[0].type == 'BRAND') {
      return await strapi.services.gig.find({ owner: profiles[0]._id });
    } else {
      return await strapi.services.gig.find({ hires: profiles[0]._id });
    }
  },

  async applySelf(ctx) {
    const { _id } = ctx.state.user;
    const profiles = await strapi.services.profile.find({ user: _id, _limit: 1 });
    const gig = await strapi.services.gig.findOne({ id: ctx.params._id });
    if (!gig || profiles.length < 1)
      return ctx.notFound();
    gig.applicants.push(profiles[0]);
    await gig.save();
    await strapi.services.device.sendPushNotification(
      [gig.owner],
      "New applicant",
      `${profiles[0].fullname} has applied for "${gig.name}" gig`,
      { code: 'GIG_NEW_APPLICANT', gigId: gig.id }
    );
    return gig;
  },

  async quitSelf(ctx) {
    const { _id } = ctx.state.user;
    const profiles = await strapi.services.profile.find({ user: _id, _limit: 1 });
    const gig = await strapi.services.gig.findOne({ id: ctx.params._id });
    if (!gig || profiles.length < 1)
      return ctx.notFound();
    const index = gig.applicants.findIndex(a => a.id == profiles[0].id);
    if (index >= 0)
      gig.applicants.splice(index, 1);
    await gig.save();
    await strapi.services.device.sendPushNotification(
      [gig.owner],
      "Applicant quit",
      `${profiles[0].fullname} has quit "${gig.name}" gig`,
      { code: 'GIG_APPLICANT_QUIT', gigId: gig.id }
    );
    return gig;
  },
};
