'use strict';

const fetch = require('node-fetch');

const geocodingUri = 'https://maps.googleapis.com/maps/api/geocode/json';

module.exports = {
  async geocode(address) {
    const apiKey = strapi.config.currentEnvironment.GOOGLE_API_KEY;
    const query = encodeURIComponent(address);
    try {
      const response = await fetch(`${geocodingUri}?key=${apiKey}&address=${query}&region=us&language=en`);
      if (!response.ok) {
        strapi.log.error('failed to geocode address, http status: ', response.status);
        return [];
      }
      const body = await response.json();
      if (body.status !== 'OK') {
        strapi.log.error('failed to geocode address, response status: ', body.status);
        return [];
      }
      return body.results.map(result => ({
        address: result.formatted_address,
        latitude: result.geometry.location.lat,
        longitude: result.geometry.location.lng,
      }));
    } catch (e) {
      strapi.log.error('failed to geocode address', e);
      return [];
    }
  },
};