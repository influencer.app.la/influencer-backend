'use strict';

const _ = require('lodash');

module.exports = {
  async find(ctx) {
    if (ctx.query._q) {
      // todo: google places api
      const results = await Promise.all([
        strapi.services.location.search(ctx.query),
        strapi.services.location.geocode(ctx.query._q),
      ]);
      return _.flatten(results);
    }
    return strapi.services.location.find(ctx.query);
  },
};
