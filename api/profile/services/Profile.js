'use strict';

const fetch = require('node-fetch');
const { URLSearchParams } = require('url');

module.exports = {
  instagram: {
    async gainAccessToken(ctx) {
      const response = await fetch('https://api.instagram.com/oauth/access_token', {
        method: 'POST',
        body: new URLSearchParams({
          "client_id": strapi.config.currentEnvironment.IG_CLIENT_ID,
          "client_secret": strapi.config.currentEnvironment.IG_CLIENT_SECRET,
          "grant_type": "authorization_code",
          "redirect_uri": ctx.origin + ctx.path,
          "code": ctx.query.code,
        }),
      });
      if (!response.ok) {
        strapi.log.error('failed to get instagram access token, status %d, error: %s',
          response.status, response.statusText);
        throw "Instagram Response Error";
      }
      return await response.json();
    },

    async loadProfile(accessToken) {
      const response = await fetch('https://api.instagram.com/v1/users/self/?access_token='
        + accessToken);
      if (!response.ok) {
        strapi.log.error('failed to get instagram profile, status %d, error: %s',
          response.status, response.statusText);
        throw "Instagram Response Error";
      }
      return (await response.json())['data'];
    },

    async loadRecentMedia(accessToken) {
      const response = await fetch('https://api.instagram.com/v1/users/self/media/recent/?access_token='
        + accessToken);
      if (!response.ok) {
        strapi.log.error('failed to get instagram recent media, status %d, error: %s',
          response.status, response.statusText);
        throw "Instagram Response Error";
      }
      return (await response.json())['data'];
    },

  }
};
