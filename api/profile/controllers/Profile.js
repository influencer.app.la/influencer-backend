'use strict';

const _ = require('lodash');

const pickFields = ['id', 'type', 'fullname', 'ig'];

module.exports = {
  async findSelf(ctx) {
    const { _id } = ctx.state.user;
    const results = await strapi.services.profile.find({ user: _id, _limit: 1 });
    if (results.length > 0)
      return _.pick(results[0], pickFields);
    return {};
  },

  async createFromInstagram(ctx) {
    try {
      const igState = ctx.query.state.split(' ');
      const userId = igState[0];
      const type = igState[1];
      const accessToken = await strapi.services.profile.instagram.
        gainAccessToken(ctx);
      const profile = await strapi.services.profile.instagram.
        loadProfile(accessToken['access_token']);
      const recentMedia = await strapi.services.profile.instagram.
        loadRecentMedia(accessToken['access_token']);
      await strapi.services.profile.create({
        user: userId,
        type,
        fullname: profile.full_name,
        igAccessToken: accessToken['access_token'],
        ig: {
          ...profile,
          recent_media: recentMedia,
        },
        igSyncedAt: new Date(),
      });
      ctx.redirect('/ig_profile_success.html');
    } catch (e) {
      return '<h1>Failed</h1>';
    }
  }
};
