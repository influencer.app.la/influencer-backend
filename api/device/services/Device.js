'use strict';

const firebaseAdmin = require('firebase-admin');
const firebaseAdminCreds = require('../../../firebase-adminsdk-config.json');

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(firebaseAdminCreds),
});

module.exports = {
  async sendPushNotification(profiles, title, body, data) {
    if (profiles.length == 0)
      return;
    try {
      if (typeof profiles[0] === 'string')
        profiles = await strapi.services.profile.find(
          { _id: { '$in': profiles } });
      const devices = await strapi.services.device.find(
        { user: { '$in': profiles.map(m => m.user._id) } });
      if (devices.length == 0)
        return strapi.log.warn('no devices were found to send push notifications to'); 
      const resp = await firebaseAdmin.messaging().sendMulticast({
        tokens: devices.map(d => d.pushToken),
        notification: { title, body },
        android: { notification: { sound: 'default' } },
        apns: { payload: { aps: { sound: 'default' } } },
        data,
      });
      strapi.log.debug('%d of %d push notifications sent successfully',
        resp.successCount, devices.length);
    } catch (e) {
      strapi.log.error('failed to send push notification: ', e);
    }
  }
};
