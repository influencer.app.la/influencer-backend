'use strict';

const _ = require('lodash');

module.exports = {

  async createOrUpdate(ctx) {
    const body = ctx.request.body;
    const query = { deviceId: body.deviceId };
    const devices = await strapi.services.device.find(query);
    if (devices.length == 0) {
      return await strapi.services.device.create(
        { ...body, user: ctx.state.user._id });
    } else {
      const device = devices[0];
      _.merge(device, body);
      device.user = ctx.state.user;
      await device.save();
      return device;
    }
  }
};
