'use strict';

const moment = require('moment');
/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK] [YEAR (optional)]
 */

module.exports = {
  '* * * * *': async () => {
    const threashold = moment().add(-1, 'day').toDate();
    const profiles = await Profile.find({ igSyncedAt: { "$lt": threashold } });
    for (const profile of profiles) {
      strapi.log.debug('updating instagram account of %s profile', profile.id);
      const ig = await strapi.services.profile.instagram.loadProfile(profile.igAccessToken);
      const recentMedia = await strapi.services.profile.instagram.loadRecentMedia(profile.igAccessToken);
      profile.ig = { ...ig, recent_media: recentMedia };
      profile.igSyncedAt = new Date();
      await profile.save();
    }
  }
};
