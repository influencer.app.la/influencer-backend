FROM node:11.15.0-slim

WORKDIR /usr/src/app

COPY ./api ./api
COPY ./config ./config
COPY ./extensions ./extensions
COPY ./public ./public
COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json
COPY ./favicon.ico ./favicon.ico
COPY ./firebase-adminsdk-config.json ./firebase-adminsdk-config.json

RUN npm install
RUN ./node_modules/.bin/strapi build

CMD ./node_modules/.bin/strapi start